﻿#include <iostream>

class Test
{
private:
    int a;
    int b;

public:
    Test() : a(0), b(0)
    {}

    Test(int _a, int _b) : a(_a), b(_b)
    {}

    void Print()
    {
        std::cout << "\na=" << a << " " << "b=" << b << "\n";
    }
};

class Vector
{
private:
    double x;
    double y;
    double z;

public:
    Vector() : x(0), y(0), z(0)
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}

    void Show()
    {
        std::cout << "\nX=" << x << "  Y=" << y << "  Z=" << z;
    }

    void VLength()
    {
        double VL = sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
        std::cout << "\nVector length: " << VL;
    }
};

int main()
{
    Test one(5, 10);
    one.Print();

    Vector Go(10, 10, 10);
    Go.Show();
    Go.VLength();

    return 0;
}
